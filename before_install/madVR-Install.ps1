foreach ($a in ${env:PATH}.Split(";")) {
    If ($(Test-Path $a\pwsh-preview.cmd)) { 
        $pwshl = $a
        $pwsha = "pwsh-preview.cmd"
	$pwver = "pwsh"
        break
    }
}
If (!([string]::IsNullOrEmpty($pwshl))) {
    Write-Host "Pwsh:", $pwshl
} Else {
    foreach ($a in ${env:PATH}.Split(";")) {
        If ($(Test-Path $a\pwsh.exe)) { 
            $pwshl = $a
            $pwsha = "pwsh.exe"
            $pwver = "pwsh"
            break
        }
    }
    Write-Host "Pwsh:", $pwshl
}

If (!($pwver = "pwsh")) {
    $pwshl = "C:\Windows\System32\WindowsPowerShell\v1.0"
    $pwsha = "powershell.exe"
    $pwver = "powershell"
    Write-Host "Powershell:", $pwshl
}

cd ..\shared\madVR\
& "${pwshl}\${pwsha}" -c Start-Process install.bat -verb runas
pause