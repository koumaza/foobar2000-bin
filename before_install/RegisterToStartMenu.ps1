###
### Configuration
###
[string]$ExecutableFilePath		=	"..\foobar2000.exe"
[string]$IconFilePath			=	"..\shared\foobar2000\Icon\4.ico"


[string]$ExecutableFileFullPath	=	Get-ChildItem ${ExecutableFilePath}	| Select-Object -ExpandProperty FullName
[string]$IconFileFullPath		=	Get-ChildItem ${IconFilePath}		| Select-Object -ExpandProperty FullName

[string]$UserStartMenuConfigurationPath	= `
									"${env:APPDATA}\Microsoft\Windows\Start Menu\Programs"
									
foreach	($P in ${env:PATH}.Split(";")) {
	foreach	($E in @("exe","ps1","cmd")) {
		If	($(Test-Path ${P}\git.${E})) {
			[string]$GitLocation	=	$(Get-ChildItem ${P}\git.${E})
			[bool]	$UseGitHash		=	$true
			break
		}
	}
	If	($UseGitHash) {
		Remove-Item -Recurse -Path "${UserStartMenuConfigurationPath}\foobar2000-bin-*"
		[string]$HexHash		=	$(& "$GitLocation" rev-parse HEAD).Substring(0, 8)
		break
	}
	[bool]	$UseGitHash			=	$false
}

If	(!($UseGitHash)) {
	[array]	$HexChar			=	(-Join ([char[]]((48..57) + (97..102)))).ToCharArray()
	[string]$HexHash			=	"$(-Join (Get-Random -Count 8 -input ${HexChar}))-NotFromGit"
}

[string]$ShortcutFileDirectory	=	New-Item -ItemType Directory ${UserStartMenuConfigurationPath}\foobar2000-bin-${HexHash}


$WSS							=	New-Object -ComObject WScript.Shell
$ShortcutLink					=	$WSS.CreateShortcut("${ShortcutFileDirectory}\foobar2000-${HexHash}.lnk")
$ShortcutLink.TargetPath		=	$ExecutableFileFullPath
$ShortcutLink.IconLocation		=	$IconFileFullPath
$ShortcutLink.Save()