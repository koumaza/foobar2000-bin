[System.Collections.ArrayList]$ExecutableFiles		=	Get-ChildItem -Name -File -Include *.exe,*.msi
[System.Collections.ArrayList]$PowershellScripts	=	Get-ChildItem -Name -File -Include *.ps1 -Exclude "RunAll.ps1"
[System.Collections.ArrayList]$ExecutableScripts	=	Get-ChildItem -Name -File -Include *.bat,*.cmd


foreach	($C in ${ExecutableFiles}) {
	& .\$C
}

foreach ($a in ${env:PATH}.Split(";")) {
	If ($(Test-Path $a\pwsh-preview.cmd)) { 
		$pwshl = $a
		$pwsha = "pwsh-preview.cmd"
		$pwver = "pwsh"
		break
	}
}
If (!([string]::IsNullOrEmpty($pwshl))) {
	Write-Host "Pwsh:", $pwshl
} Else {
	foreach ($a in ${env:PATH}.Split(";")) {
		If ($(Test-Path $a\pwsh.exe)) { 
			$pwshl = $a
			$pwsha = "pwsh.exe"
			$pwver = "pwsh"
			break
		}
	}
	Write-Host "Pwsh:", $pwshl
}
If (!($pwver = "pwsh")) {
	$pwshl = "C:\Windows\System32\WindowsPowerShell\v1.0"
	$pwsha = "powershell.exe"
	$pwver = "powershell"
	Write-Host "Powershell:", $pwshl
}

foreach	($C in ${PowershellScripts}) {
	& "${pwshl}\${pwsha}" .\$C
}

foreach	($C in ${ExecutableScripts}) {
	& "${env:WINDIR}\System32\cmd.exe" /c .\$C
}