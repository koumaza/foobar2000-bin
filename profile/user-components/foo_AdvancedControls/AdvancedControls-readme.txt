Advanced Controls v0.6.7.4 component for Foobar2000

System Requirements:
   Windows XP, Vista, Windows 7*
   Foobar2000 v1.1+


*  In Windows 7, due to major changes in the GUI interaction possibilities,
   the behavior is a bit different.

   - The first time you run Foobar2000 with Advanced Controls, you will
     have to move the two icons in place. Unfortunately the plugin can't do
     that for you. Click the small arrow of the Taskbar Notification Area
     (the tray) to open the Notification Area Overflow. Drag the two icons
     out of this area and place them where you want them to be. Make sure
     the two icons are in correct order: |< ^ > >|. The icons will NOT work
     properly from the overflow area or if they aren't in the correct order,
     next to each other.

     Windows will then remember their position for the next times you use
     Foobar2000.

   - To use the position/volume slider, you need to use the RIGHT mouse
     button, as the left button is reserved by Windows for moving the icons
     themselves.

Control all the playback functions of Foobar2000 from icons in the Taskbar
Notification Area.

In a very small space (2 tray icons wide), you have the playback functions
(Play / Pause, Stop / Load, Previous, Next) and the volume or the seeking bar.
Plus, a right-click menu with all these functions, and more!

A popup balloon appears each time a new song begins, to tell the title of
the song. The balloon can be disabled, and the info displayed is customizable
with the advanced title formatting provided in the Titles Formatting
preferences. The balloon is not available in Windows 95 and 98.

The Delete Current File function deletes the file that is currently playing. It
always asks for confirmation and put the file into the recycle bien if possible,
so you won't delete files without intending to.

The skins of Foobar2000 Advanced Controls are compatible with the skins of
Winamp Advanced Controls (http://www.niversoft.com/winamp)

Try it, you will like it.

Nicolas Hatier
Niversoft id�es logicielles
http://www.niversoft.com
mailto://info@niversoft.com
