Natural headphone crossfeed for foobar2000 v1.1
Readme

Natural crossfeed designed to give a realistic crossfeed / speaker simulation effect on headphones / earphones while producing a wide soundstage and zero sound coloration.

Simulates speakers spaced 30+30 degrees apart (classic stereo setup with better front projection of imaging) or 50+50degrees apart (widened stereo setup with more similarity to classic headphone soundstage width)

A. Installation

1. Copy foo_dsp_stereoconv.dll to the "components" directory of your foobar2000 install.
2. Copy the two bundled wave files to a conveniently locatable directory (e.g. foobar2000 main directory)
3. Restart foobar2000 (if already running)

B. Loading the DSP

1. Go to Preferences->Playback->DSP Manager.
2. Put Resampler(dBpoweramp/SSRC) or Resampler(PPHS) onto the list of active DSPs.  This is necessary because convolution works at a fixed sample rate.  SSRC is slightly preferable to PPHS if available.
3. Click on Resampler and click "Configure selected" to configure the resampler.  Configure the resampler to resample to 44100Hz.  If using Resampler(PPHS), tick the checkbox for "Ultra mode".
4. Put Stereo Convolver next on the list of active DSPs.  Click on "Stereo Convolver" and click "Configure selected" to configure it.  For "Left Wav File" browse for "Left (30deg/50deg) 44100.wav".  For "Right Wav file" browse for "Right (30deg/50deg) 44100.wav".  For a volume matched with unprocessed sound set Level Adjustment to 0.0dB.
5. Press OK on the config window, then OK on the main Preferences window to save changes.
6. You may later come back to the DSP Preferences window and save these settings in a new "DSP chain preset".

C. Playing music and listening

1. *It is advised to set the master volume of foobar to -4dB or below to avoid possible clipping produced by the increased digital levels variation (de-compression effect) caused by the crossfeed processing.  Turn up the volume elsewhere (e.g. on your amplifier) to compensate.*
2. While playing music, you can double-click on "Stereo Convolver" to take it in and out of the active DSP chain to verify the effect it is having on the music.  *But don't click on OK or Apply while it's out of the chain, otherwise the settings will be reset.*
3. It should be a subtle effect, sound should be slightly more forward and the left-centre-right blobs effect of headphone soundstage should be alleviated.  At no point should the soundstage feel constricted in width.  Indeed the soundstage should continue to be a very enveloping experience and you may at times struggle to identify whether the crossfeed is slotted in or out.

D. Changelog
2016-06-22 v1.1 Added impulses for virtual speakers spaced 30+30 degrees apart (classic stereo setup with better front projection of imaging)
2016-06-21 v1.0 Initial release with impulses for virtual speakers spaced 50+50 degrees apart (widened stereo setup with more similarity to classic headphone soundstage width)

E. Credits
+Eric89GXL of hydrogenaudio.org for creating the stereo crossfeed-convolution foobar2000 plugin
+Impulses designed with the aid of Wave Arts Panorama and Audacity for waveform editing, Microsoft Excel for statistical analysis, and Voxengo CurveEQ for applying changes based on analysis
+Natural headphone crossfeed for foobar2000 v1.0 package designed by Joseph Yeung (aka Joe Bloggs on Head-Fi)